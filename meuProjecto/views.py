from django.http import HttpResponse
from django.shortcuts import render

def hello(request):
    # return HttpResponse("Ola mundo")
    return render(request, 'index.html')

def fnome(request, nome):
    return render(request, 'pessoa.html', {nome})

